= One Time Key Authenticate =

This module provides a authentication provider and a service that allows you to
generate a key to authenticate a user exactly once.

The authentication provide is global, like "Cookie" authentication that comes with core's user module.

== WARNING ==

Generating keys for user 1 (the super user account) will let people with that key do anything the super user account can do.
While this is the whole point of this module, you should use it carefully.

== How to use ==

Step 1:
Ask the service for a key:
<code>
$uid = 'The uid of your favourite user';
$key = \Drupal->service('one_time_key_auth')->generateKeyFor($uid);
</code>

Step 2:

Make a request to Drupal with the GET or POST value of "otka=$key".

Once the key is use, it will no longer be valid.
