<?php

namespace Drupal\one_time_key_auth\Authentication\Provider;

use Drupal\one_time_key_auth\OneTimeKeyAuthService;
use Drupal\Core\Authentication\AuthenticationProviderInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Key authentication provider.
 */
class OneTimeKeyAuth implements AuthenticationProviderInterface {
  protected $otka;

  /**
   * {@inheritdoc}
   */
  public function __construct(OneTimeKeyAuthService $otka) {
    $this->otka = $otka;
  }

  /**
   * {@inheritdoc}
   */
  public function applies(Request $request) {
    return (bool) $this->otka->extractKey($request);
  }

  /**
   * {@inheritdoc}
   */
  public function authenticate(Request $request) {
    // Get the provided key.
    if ($key = $this->otka->extractKey($request)) {
      // Find the linked user and consume the key.
      if ($user = $this->otka->consumeKey($key)) {
        return $user;
      }
    }
    return NULL;
  }

}
