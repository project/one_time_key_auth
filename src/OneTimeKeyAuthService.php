<?php

namespace Drupal\one_time_key_auth;

use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Component\Utility\Crypt;
use Symfony\Component\HttpFoundation\Request;

/**
 * One Time Key authenticate service.
 */
class OneTimeKeyAuthService {
  protected $database;
  protected $entityTypeManager;

  /**
   * Constructor for the service.
   */
  public function __construct(Connection $database, EntityTypeManagerInterface $entityTypeManager) {
    $this->database = $database;
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * Consume a key and get the user that is associated with that key.
   */
  public function consumeKey($key) {
    $user_storage = $this->entityTypeManager->getStorage('user');

    $this->database->delete('one_time_key_auth')
      ->condition('expires', time(), '<')
      ->execute();

    $records = $this->database->select('one_time_key_auth', 'otka')
      ->fields('otka', ['uid', 'key', 'expires'])
      ->condition('otka.expires', time(), '>')
      ->condition('otka.key', $key)
      ->execute();

    $uid = FALSE;
    foreach ($records as $record) {
      $uid = $record->uid;
    }

    if ($uid) {
      $this->database->delete('one_time_key_auth')
        ->condition('key', $key)
        ->execute();

      $user = $user_storage->load($uid);
      if ($user) {
        return $user;
      }
    }

    return FALSE;
  }

  /**
   * Generate a One time use key for a user.
   */
  public function generateKeyFor($uid) {
    $key = bin2hex(Crypt::randomBytes(32));
    // Expire key in 15 minutes.
    $expires = time() + (60 * 15);
    $transaction = $this->database->startTransaction();
    try {
      $this->database
        ->insert('one_time_key_auth')
        ->fields([
          'uid' => $uid,
          'key' => $key,
          'expires' => $expires,
        ])->execute();
    }
    catch (Exception $e) {
      $transaction->rollBack();
      return FALSE;
    }
    return $key;
  }

  /**
   * Load the key from the request.
   */
  public function extractKey(Request $request) {
    return $request->query->get('otka') ?? FALSE;
  }

}
