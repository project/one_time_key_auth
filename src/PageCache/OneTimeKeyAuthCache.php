<?php

namespace Drupal\one_time_key_auth\PageCache;

use Drupal\one_time_key_auth\OneTimeKeyAuthService;
use Drupal\Core\PageCache\RequestPolicyInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Cache policy one time key auth.
 *
 * Block any caching if authentication is done via one time key auth.
 */
class OneTimeKeyAuthCache implements RequestPolicyInterface {
  protected $otka;

  /**
   * {@inheritdoc}
   */
  public function __construct(OneTimeKeyAuthService $otka) {
    $this->otka = $otka;
  }

  /**
   * {@inheritdoc}
   */
  public function check(Request $request) {
    if ($this->otka->extractKey($request)) {
      return self::DENY;
    }
    return NULL;
  }

}
